package model.data_structures;

public class Node<E> {
	
    private Node<E> siguiente;
    private E elemento;
    
    public Node(E pElemento) {
    	
    	siguiente = null;
    	this.elemento = pElemento;
    }
    
    public Node<E> darSiguiente( ){
    	
    	return siguiente;
    	
    }
    
    public void cambiarSiguiente( Node<E> pSiguiente ){
    	
    	this.siguiente = pSiguiente;
    }
    
    public E darElemento( ){
    	
    	return elemento;
    }
    
    public void cambiarElemento( E pElemento){
    	
    	this.elemento = pElemento;
    }
    
}